#!/usr/bin/env ruby
require_relative 'lib/greeter'
require 'byebug'


greeter1 = Greeter.new("Lendahand")
greeter2 = Greeter.new("Plusplus")
greeter3 = Greeter.new("Acme")

puts "Who are you?"
name = gets.chomp # Assignment

[greeter1, greeter2, greeter3].each do |greeter|
  greeter.greet(name)
end

