require 'paint'

class Greeter
  def initialize(company)
    @company = company
  end

  def greet(name)
    puts "Hello #{name} at #{Paint[@company, color]}"
  end

  def color
    case @company
    when "Lendahand"
      :green
    when "Plusplus"
      :red
    else
      :black
    end
  end
end
